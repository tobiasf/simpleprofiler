from time import sleep
from simpleprofiler import profiler

profiler.tick('foo')
profiler.tick('bar')
profiler.tick('bar')
profiler.tick('bar')
sleep(1)
profiler.tick('bar')
profiler.tick('this is a very long label, but it still looks good (sort of)')
sleep(1)
profiler.tick('this is a very long label, but it still looks good (sort of)')
profiler.tick('foo')
print(profiler)
