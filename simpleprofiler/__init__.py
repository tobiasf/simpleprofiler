from copy import deepcopy
from datetime import datetime
from collections import defaultdict

import logging
logger = logging.getLogger('simpleprofiler')


class Table(object):
    def __init__(self, headers):
        self.headers = headers
        self.rows = []
        self.column_formatters = {
            'label': str,
            'calls': self._format_calls,
            'total': self._format_time,
            'avg': self._format_time,
            'min': self._format_time,
            'max': self._format_time,
            'median': self._format_time,
        }
        self._column_widths = {}

    def add_row(self, row):
        self.rows.append(row)

    def _format_calls(self, calls):
        if calls > 1000 * 1000 * 1000:
            logger.warning(f'Cannot render calls > {1000 * 1000 * 1000}')
            return -1
        if calls > 1000 * 1000:
            return f'{round(calls/1000000.0, 1)}m'
        if calls > 1000:
            return f'{round(calls/1000.0, 1)}k'
        return calls

    def _format_time(self, seconds):
        return "{0:.2f}s".format(seconds)

    def _format_rows(self):
        for row in self.rows:
            for header, formatter in self.column_formatters.items():
                row[header] = formatter(row[header])

    def _calculate_column_widths(self):
        for header in self.headers:
            self._column_widths[header] = max([len(str(row[header])) for row in self.rows] + [len(header)])

    def _str_row(self, row):
        def _add_spaces(value, maxlen):
            value = str(value)
            return value + ''.join([' ' for i in range(maxlen - len(value))])
        return '  '.join([_add_spaces(row[header], self._column_widths[header]) for header in self.headers])

    def __str__(self):
        self._format_rows()
        self._calculate_column_widths()
        s = [self._str_row({h:h for h in self.headers})]
        for row in self.rows:
            s.append(self._str_row(row))
        return '\n'.join(s)


class Profiler(object):

    def __init__(self, sort_by):
        self.reset()
        self._columns = (
            "label", "calls", "total", "min", "max", "avg", "median",
        )
        self._column_widths = {}
        assert sort_by in self._columns,  f'sort_by must be one of: "{self._columns}"'
        self.sort_by = sort_by

    def reset(self):
        self._times = defaultdict(list)
        self._start_times = defaultdict(lambda: 0)

    def tick(self, label):
        if label in self._start_times:
            self._times[label].append((datetime.utcnow() - self._start_times.pop(label)).total_seconds())
            return
        self._start_times[label] = datetime.utcnow()

    def _stats(self, label):
        result = {c: None for c in self._columns}
        lst = sorted(self._times[label])
        if not lst:
            return result

        lst_length = len(lst)
        total = sum(lst)
        result['total'] = total
        result['avg'] = total / lst_length
        result['min'] = lst[0]
        result['max'] = lst[-1]
        result['calls'] = lst_length
        result['label'] = label

        index = (lst_length - 1) // 2
        if (lst_length % 2):
            result['median'] = lst[index]
        else:
            result['median'] = (lst[index] + lst[index + 1]) / 2.0
        result['median'] = result['median']
        return result

    def __str__(self):
        divider = '=' * 80
        s = [divider[:10] + ' Profiler ' + divider[20:]]

        t = Table(self._columns)
        for row in sorted([self._stats(k) for k in self._times], key=lambda _s: _s[self.sort_by]):
            t.add_row(row)

        s.append(str(t))
        s.append(divider)
        return '\n'.join(s)

profiler = Profiler(sort_by='label')
