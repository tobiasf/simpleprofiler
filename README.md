**Python3 only**

## Installation ##

```
pip install git+https://bitbucket.org/tobiasf/simpleprofiler
```

## Usage ##


```
#!python

from time import sleep
from simpleprofiler import profiler

profiler.tick('foo')
profiler.tick('bar')
profiler.tick('bar')
profiler.tick('bar')
sleep(1)
profiler.tick('bar')
profiler.tick('this is a very long label, but it still looks good (sort of)')
sleep(1)
profiler.tick('this is a very long label, but it still looks good (sort of)')
profiler.tick('foo')
print(profiler)
```


## Output ##

```
#!python


========== Profiler ============================================================
label                                                         calls  total  min    max    avg    median
bar                                                           2      1.00s  0.00s  1.00s  0.50s  0.50s
foo                                                           1      2.00s  2.00s  2.00s  2.00s  2.00s
this is a very long label, but it still looks good (sort of)  1      1.00s  1.00s  1.00s  1.00s  1.00s
================================================================================
```
