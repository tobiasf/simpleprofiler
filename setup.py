"""A very simple profiler
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='simpleprofiler',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='1.0.3',

    description='A very simple python profiler',
    long_description=long_description,

    url='https://bitbucket.org/tobiasf/simpleprofiler',

    # Author details
    author='Tobias Fielitz',
    author_email='<first_name>.<last_name>@gmail.com',

    # Choose your license
    license='MIT',

    # What does your project relate to?
    keywords='profiler profiling',

    packages=['simpleprofiler'],
)
